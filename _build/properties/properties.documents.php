<?php

$properties = array();

$tmp = array(
	'classKey'       => array(
		'type' => 'textfield',
		'value' => '',
	),
	'classKeyId'     => array(
		'type' => 'textfield',
		'value' => '',
	),
	'DocTypeListRow' => array(
		'type'  => 'textfield',
		'value' => 'tpl.Document.DocTypeListRow',
	),
	'DocTypeList'    => array(
		'type' => 'textfield',
		'value' => 'tpl.Document.DocTypeList',
	),
	'DocForm'        => array(
		'type'  => 'textfield',
		'value' => "tpl.Document.DocForm",
	),
);

foreach ($tmp as $k => $v) {
	$properties[] = array_merge(
		array(
			'name' => $k,
			'desc' => PKG_NAME_LOWER . '_prop_' . $k,
			'lexicon' => PKG_NAME_LOWER . ':properties',
		), $v
	);
}

return $properties;