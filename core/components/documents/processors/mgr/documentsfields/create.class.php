<?php

/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 24.03.2016
 * Time: 16:30
 */
class DocumentsTypeFieldsCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'DocumentTypeField';
    public $classKey = 'DocumentTypeField';
    public $languageTopics = array('documents');
    //public $permission = 'create';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $name = trim($this->getProperty('field'));
        if (empty($name)) {
            $this->modx->error->addField('field', $this->modx->lexicon('documents_item_err_name'));
        } elseif ($this->modx->getCount($this->classKey, array('field' => $name))) {
            $this->modx->error->addField('field', $this->modx->lexicon('documents_item_err_ae'));
        }

        return parent::beforeSet();
    }

}

return 'DocumentsTypeFieldsCreateProcessor';