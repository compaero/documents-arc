<?php

/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 24.03.2016
 * Time: 18:42
 */
class DocumentsTypesFieldsGetListProcessor extends modObjectGetListProcessor
{
    public $objectType = 'DocumentTypeFieldLink';
    public $classKey = 'DocumentTypeFieldLink';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    //public $permission = 'list';


    /**
     * * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return boolean|string
     */
    public function beforeQuery()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $id = trim($this->getProperty('id'));
        if ($id) {
            $c->where(array(
                'type_id' => $id,

            ));
        }


        return $c;
    }


    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $array = $object->toArray();
        $array['actions'] = array();

        // Edit
        $array['actions'][] = array(
            'cls'    => '',
            'icon'   => 'icon icon-edit',
            'title'  => $this->modx->lexicon('documents_item_update'),
            //'multiple' => $this->modx->lexicon('documents_items_update'),
            'action' => 'updateItem',
            'button' => true,
            'menu'   => true,
        );

        if (!$array['active']) {
            $array['actions'][] = array(
                'cls'      => '',
                'icon'     => 'icon icon-power-off action-green',
                'title'    => $this->modx->lexicon('documents_item_enable'),
                'multiple' => $this->modx->lexicon('documents_items_enable'),
                'action'   => 'enableItem',
                'button'   => true,
                'menu'     => true,
            );
        } else {
            $array['actions'][] = array(
                'cls'      => '',
                'icon'     => 'icon icon-power-off action-gray',
                'title'    => $this->modx->lexicon('documents_item_disable'),
                'multiple' => $this->modx->lexicon('documents_items_disable'),
                'action'   => 'disableItem',
                'button'   => true,
                'menu'     => true,
            );
        }

        // Remove
        $array['actions'][] = array(
            'cls'      => '',
            'icon'     => 'icon icon-trash-o action-red',
            'title'    => $this->modx->lexicon('documents_item_remove'),
            'multiple' => $this->modx->lexicon('documents_items_remove'),
            'action'   => 'removeItem',
            'button'   => true,
            'menu'     => true,
        );
        if($field_id = $this->modx->getObject('DocumentTypeField',array('id'=>$array['field_id']))){

            $array['field_id'] = $field_id->get('field');
        };
        return $array;
    }

}

return 'DocumentsTypesFieldsGetListProcessor';