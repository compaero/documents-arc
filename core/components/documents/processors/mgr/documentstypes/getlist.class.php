<?php

/**
 * Get a list of Items
 */
class DocumentsTypesGetListProcessor extends modObjectGetListProcessor
{
    public $objectType = 'DocumentType';
    public $classKey = 'DocumentType';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    //public $permission = 'list';

    /**
     * * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return boolean|string
     */




    public function beforeQuery()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = trim($this->getProperty('query'));
        if ($query) {
            $c->where(array(
                'type:LIKE'           => "%{$query}%",
                'OR:id:LIKE' => "%{$query}%",
            ));
        }
        $c->prepare();

        return $c;
    }


    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $array = $object->toArray();
        $array['actions'] = array();

        // Edit
        $array['actions'][] = array(
            'cls'    => '',
            'icon'   => 'icon icon-edit',
            'title'  => $this->modx->lexicon('documents_item_update'),
            //'multiple' => $this->modx->lexicon('documents_items_update'),
            'action' => 'updateItem',
            'button' => true,
            'menu'   => true,
        );

        if (!$array['active']) {
            $array['actions'][] = array(
                'cls'      => '',
                'icon'     => 'icon icon-power-off action-green',
                'title'    => $this->modx->lexicon('documents_item_enable'),
                'multiple' => $this->modx->lexicon('documents_items_enable'),
                'action'   => 'enableItem',
                'button'   => true,
                'menu'     => true,
            );
        } else {
            $array['actions'][] = array(
                'cls'      => '',
                'icon'     => 'icon icon-power-off action-gray',
                'title'    => $this->modx->lexicon('documents_item_disable'),
                'multiple' => $this->modx->lexicon('documents_items_disable'),
                'action'   => 'disableItem',
                'button'   => true,
                'menu'     => true,
            );
        }

        // Remove
        $array['actions'][] = array(
            'cls'      => '',
            'icon'     => 'icon icon-trash-o action-red',
            'title'    => $this->modx->lexicon('documents_item_remove'),
            'multiple' => $this->modx->lexicon('documents_items_remove'),
            'action'   => 'removeItem',
            'button'   => true,
            'menu'     => true,
        );

     if($parent = $this->modx->getObject('DocumentType',array('id'=>$array['parent'])) and $array['parent']!=0){

         $array['parent'] = $parent->get('type');
    }
        else{
            $array['parent'] = '';
        };
        return $array;
    }


}

return 'DocumentsTypesGetListProcessor';