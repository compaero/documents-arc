<?php

/**
 * Update an Item
 */
class DocumentsTypeUpdateProcessor extends modObjectUpdateProcessor
{
    public $objectType = 'DocumentType';
    public $classKey = 'DocumentType';
    public $languageTopics = array('documents');
    public $primaryKeyField = 'id';

    //public $permission = 'save';

    public function initialize()
    {
        $data = $this->getProperties();
        if (is_array($data['data'])) {

        } else {
            $data = $this->modx->fromJSON($data['data']);
            foreach ($data as $key => $prop) {
                $this->setProperty($key, $prop);
            }
        }


        return parent::initialize();
    }

    /**
     * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return bool|string
     */
    public function beforeSave()
    {

        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @return bool
     */

}

return 'DocumentsTypeUpdateProcessor';
