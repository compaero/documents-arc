<?php

/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 25.03.2016
 * Time: 16:21
 */
class DocumentsTypelinksGetProcessor extends modObjectUpdateProcessor
{
    public $objectType = 'DocumentsTypeLink';
    public $classKey = 'DocumentsTypeLink';
    public $languageTopics = array('documents');
    public $primaryKeyField = 'id';

    //public $permission = 'save';

    public function initialize()
    {
        $data = $this->getProperties();
        if (is_array($data['data'])) {

        } else {
            $data = $this->modx->fromJSON($data['data']);
            foreach ($data as $key => $prop) {
                $this->setProperty($key, $prop);
            }
        }


        return parent::initialize();
    }

    /**
     * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return bool|string
     */
    public function beforeSave()
    {

        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @return bool
     */
    public function cleanup()
    {
        $array = $this->object;
        $array = $array->toArray();
        if ($doc_type_id = $this->modx->getObject('DocumentType', array('id' => $array['doc_type_id']))) {

            $array['doc_type_id'] = $doc_type_id->get('type');
        };
        $this->object = $array;
        return parent::cleanup();
    }
}

return 'DocumentsTypelinksGetProcessor';
