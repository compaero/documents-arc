<?php

/**
 * The base class for Documents.
 */
class Documents
{
    /* @var modX $modx */
    public $modx;

    public $request = array();

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('documents_core_path', $config,
            $this->modx->getOption('core_path') . 'components/documents/');
        $assetsUrl = $this->modx->getOption('documents_assets_url', $config,
            $this->modx->getOption('assets_url') . 'components/documents/');
        $connectorUrl = $assetsUrl . 'connector.php';

        $this->config = array_merge(array(
            'assetsUrl'      => $assetsUrl,
            'cssUrl'         => $assetsUrl . 'css/',
            'jsUrl'          => $assetsUrl . 'js/',
            'imagesUrl'      => $assetsUrl . 'images/',
            'connectorUrl'   => $connectorUrl,
            'json_response'  => true,
            'corePath'       => $corePath,
            'modelPath'      => $corePath . 'model/',
            'chunksPath'     => $corePath . 'elements/chunks/',
            'templatesPath'  => $corePath . 'elements/templates/',
            'chunkSuffix'    => '.chunk.tpl',
            'snippetsPath'   => $corePath . 'elements/snippets/',
            'processorsPath' => $corePath . 'processors/',
            'frontend_css' => $assetsUrl.'css/documents.css',
            'frontend_js' => $assetsUrl.'js/documents.js',
            'fotorama_js' => $assetsUrl.'fotorama/fotorama.js',
            'fotorama_css' => $assetsUrl.'fotorama/fotorama.css',
            'jgrowl'=>$assetsUrl.'css/jquery.jgrowl.min.css',

        ), $config);

        $this->modx->addPackage('documents', $this->config['modelPath']);
        $this->modx->lexicon->load('documents:default');
        if (!(isset($_POST['doc_action']) and $this->request = json_decode($_POST['doc_action'], 1))) {
            $this->request = $_POST;
        }
        $mediaSource = $this->modx->getObject('sources.modMediaSource', array(
            'name' => 'documents files'
        ));
        $properties = $mediaSource->getProperties();
        $this->pathmed = $properties['basePath']['value'];
        $exts = $properties['imageExtensions']['value'];
        $this->exts = explode(',', $exts);

    }
    public function initialize() {
        $this->loadJsCss();
        return true;
    }

    public function loadJsCss($objectName = 'documents') {

        if ($css = trim($this->config['frontend_css'])) {
            if (preg_match('/\.css/i', $css)) {
                $this->modx->regClientCSS(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $css));
            }
        }
        if ($js = trim($this->config['frontend_js'])) {
            if (preg_match('/\.js$/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        if ($fotoramajs = trim($this->config['fotorama_js'])) {
            if (preg_match('/\.js$/i', $fotoramajs)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $fotoramajs));
            }
        }
        if ($fotoramacss = trim($this->config['fotorama_css'])) {
            if (preg_match('/\.css/i', $fotoramacss)) {
                $this->modx->regClientCSS(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $fotoramacss));
            }
        }
        if ($jgrowl = trim($this->config['jgrowl'])) {
            if (preg_match('/\.css/i', $jgrowl)) {
                $this->modx->regClientCSS(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $jgrowl));
            }
        }

        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl']),
            'closeMessage' => $this->config['closeMessage'],
            'formSelector' => "form.{$this->config['formSelector']}",
            'pageId' => !empty($this->modx->resource)
                ? $this->modx->resource->get('id')
                : 0,
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript("<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true);
    }

    public function success($message = '', $data = array())
    {
        return $this->response('true', $message, $data);
    }

    public function error($message = '', $data = array())
    {
        return $this->response('false', $message, $data);
    }

    public function response($success, $message = '', $data = array())
    {
        if (empty($data)) {
            $data = 'false';
        }
        $response = array(
            'success' => $success,
            'message' => $message, //$this->modx->lexicon($message, $placeholders),
            'data'    => $data,
//			'userId' => $this->user->get('id'),
//			'PHPSESSID' => session_id(),
        );
        return $this->config['json_response'] ? $this->modx->toJSON($response) : $response;
    }

    public function NewDoc()
    {
        $data = $this->request;
        $filemax = '';
        $Doc = $this->modx->getObject('DocumentType', array('id' => $data['id']));
        $FiedLinks = $Doc->getMany('FieldLinks');


        foreach ($FiedLinks as $FiedLink) {

            if ($typefields = $FiedLink->getOne('Field')) {
                $fields[] = $typefields->toArray();
            }
        }

        $result = '';
        foreach ($fields as $val) {
            $name = $this->modx->lexicon('documents_field_' . $val['field']);
            $result .= $this->modx->getChunk('tpl.Document.DocFormFieldRow',
                array('name' => $name, 'value' => '', 'field' => $val['field'], 'type' => $val['type']));
        }

        $result = $this->modx->getChunk('tpl.Document.DocFormField',
            array('fields' => $result));

        if ($Doc->get('max_files') > 0) {
            $filemax = $Doc->get('max_files');
        }

        return $this->success('', array('formfields' => $result, 'filemax' => $filemax));
    }

    public function SaveDoc()
    {
        $classKey = $_SESSION['documentsForm']['classKey'];
        $classKeyId = $_SESSION['documentsForm']['classKeyId'];

        if($user = $this->modx->getAuthenticatedUser()){
            $surname = $user->get('surname');
        };
        $data = $this->request;

        if ($data['doc_id']) {

            if ($doc = $this->modx->getObject('Document', array('id' => $data['doc_id']))) {


                foreach ($data['data'] as $key => $val) {
                    $newdata[$val['name']] = $val['value'];

                }


                $field = $this->modx->toJSON($newdata);
                $properties = array(
                    'id'       => $data['doc_id'],
                    'field'    => $field,
                    'editedby' => $surname,
                    'editedon' => time(),
                );


                $response = $this->modx->runProcessor('update', $properties,
                    array('processors_path' => $this->config['processorsPath']));


            }

        } else {

            $createdon = time();
            $typeid = $data['typeid'];


            if ($type = $this->modx->getObject('DocumentType', array('id' => $typeid))) {


//                    $type = $type->getMany('FieldLinks');
//                    foreach($type as $FiedLink){
//
//                        if($typefields = $FiedLink->getOne('Field')){
//                            $fields[] = $typefields->toArray();
//                        }
//                    }


//                    $typeField = $type->getMany('fields');
//                    $typeField = $this->modx->fromJSON($typeField);
                $newdata = array();


                foreach ($data['data'] as $key => $val) {
                    $newdata[$val['name']] = $val['value'];

                    }


                    $field = $this->modx->toJSON($newdata);

                    $properties = array(
                        'type_id'     => $typeid,
                        'createdon'   => $createdon,
                        'field'       => $field,
                        'createdby'   => $surname,
                        'classKey'    => $classKey,
                        'classKey_id' => $classKeyId,
                        'parent' => $data['parent'],
                    );


                    $response = $this->modx->runProcessor('create', $properties,
                        array('processors_path' => $this->config['processorsPath']));

                }



        }
        if ($response->response['success']) {
            return $this->success($this->modx->lexicon('documents_save'), $response->response['object']);
        } else {
            return $this->error($this->modx->lexicon('documents_cant_save'), $response->response['message']);

        }


    }

    public function getExtension3($filename)
    {
        return substr($filename, strrpos($filename, '.') + 1);
    }

    public function phpthumb($path, $file)
    {
        ini_set("memory_limit", "256M");
        if (!class_exists('modPhpThumb')) {
            /** @noinspection PhpIncludeInspection */
            require MODX_CORE_PATH . 'model/phpthumb/modphpthumb.class.php';
        }

        $src = MODX_BASE_PATH . $path . $file;    // Исходник

        $dst = MODX_BASE_PATH . $path . 'thumb_' . $file;    // Уменьшенная копия
        $params = array(
            'w' => 300,        // Ширина
            'h' => 176,        // Высота
            // 'bg' => 'ffffff',        // Фон
            'q' => 95,            // Качество в %
            'f' => 'jpg',        // Формат изображения
        );
        // Подключаем обёртку MODX, которая выставит системные настройки в класс phpThumb
//$phpThumb = $this->modx->getService('modphpthumb','modPhpThumb', MODX_CORE_PATH . 'model/phpthumb/', array());
        $phpThumb = new modPhpThumb($this->modx);
        $phpThumb->initialize();

// Указываем исходник
        $phpThumb->setSourceFilename($src);

// Выставляем параметры
        foreach ($params as $k => $v) {
            $phpThumb->setParameter($k, $v);
        }

// Генерируем уменьшенную копию
        if ($phpThumb->GenerateThumbnail()) {
            // Выводим готовое изображение сразу на экран
            //return $phpThumb->OutputThumbnail();

            // Или сохраняем в файл
            if (!$phpThumb->renderToFile($dst)) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, 'Could not save rendered image to' . $dst);
            } else {

                return $path . 'thumb_' . $file;
            }
        } else {

            // Если возникла ошибка - пишем лог работы в журнал MODX
            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($phpThumb->debugmessages, 1));

        }


    }

    public function UploadFiles()
    {

        $data = $this->request;

        if (!$source = $this->modx->getObject('sources.modFileMediaSource', array('name' => 'documents files'))) {
            return "Couldnt get Media Source documents files";
        }
        $classKey = $_SESSION['documentsForm']['classKey'];
        $classKey_id = $_SESSION['documentsForm']['classKeyId'];
        if ($type = $this->modx->getObject('DocumentType', array('id' => $data['type_id']))) {
            $type = $type->get('type');
        }
        $uploaddir = $classKey . '/';
        $uploaddirid = $uploaddir . $classKey_id . '/';
        $uploaddirtype = $uploaddirid . $type . '/';


        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddir)) {

            mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddir);

        }

        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddirid)) {

            mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddirid);


        }
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddirtype)) {

            mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $uploaddirtype);

        }

        $source->initialize();
        foreach ($_FILES as $file) {

            $response = $source->uploadObjectsToContainer($uploaddirtype, array($file));

            if ($response) {
                $ext = $this->getExtension3($file['name']);

                $properties = array(
                    'doc_id'    => $data['id'],
                    'loaddate'  => time(),
                    'file_name' => $file['name'],
                    'path_file' => $uploaddirtype,
                    'ext'       => $ext,
                );
                $result = $this->modx->runProcessor('files/create', $properties,
                    array('processors_path' => $this->config['processorsPath']));

                if (in_array($ext, $this->exts)) {
                    $this->phpthumb($this->pathmed . $uploaddirtype, $file['name']);
                }

            }
        }
        if ($response) {
            return $this->success($this->modx->lexicon('documents_files_save'), '');
        } else {
            return $this->error($this->modx->lexicon('documents_files_err_save'), $source->getErrors());
        }
    }

    public function OpenDoc()
    {

        $data = $this->request;
        $pathfile = array();
        $docfile = '';
        $thumbfile = array();
        $result = '';
        $subtype = '';
        $docId = '';
        $subdocs = '';
        $classKey = $_SESSION['documentsForm']['classKey'];
        $classKeyId = $_SESSION['documentsForm']['classKeyId'];

        if ($Doc = $this->modx->getObject('Document', array('id' => $data['id']))) {


            $fields = $Doc->getMany('Fields');
            foreach ($fields as $field) {
                $Docfield = $field->getOne('field');
                $type = $Docfield->get('type');
                $fieldname = $Docfield->get('field');
                $name = $this->modx->lexicon('documents_field_' . $fieldname);

                $result .= $this->modx->getChunk('tpl.Document.DocFormFieldRow',
                    array('name' => $name, 'field' => $fieldname, 'type' => $type, 'value' => $field->get('value')));

            }


            $files = $Doc->getMany('Files');

            foreach ($files as $file) {

                $path = $file->get('path_file');

                $name = $file->get('file_name');
                $ext = $file->get('ext');
                if (in_array($ext, $this->exts)) {
                    $pathfile[] = $this->pathmed . $path . $name;
                    $thumbfile[] = $this->pathmed . $path . 'thumb_' . $name;
                } else {
                    $docfile .= $this->modx->getChunk('tpl.Document.DocNoImageFile',
                        array('path' => $this->pathmed . $path . $name, 'name' => $name));
                }

            }


            $result = $this->modx->getChunk('tpl.Document.DocFormField',
                array('fields' => $result));


            if ($DocType = $Doc->getOne('Type')) {
                $filemax = $DocType->get('max_files');
                $filemax = $filemax - count($Doc->getMany('Files'));
                $parenttype = $DocType->get('id');
            }

            $response = $this->modx->runProcessor('type/getlist',
                array('parent' => $parenttype, 'classKey' => $classKey),
                array('processors_path' => $this->config['processorsPath']));
            $response = $this->modx->fromJSON($response->response);

            if ($response['total'] > 0) {
                $newformdoc = 1;
                $subtype = array();
                foreach ($response['results'] as $types) {
                    $subtype .= $this->modx->getChunk('tpl.Document.DocTypeListRow',
                        array('value' => $types['id'], 'type' => $types['type']));
                    //$subtype[$types['id']] = $types['type'];
                }
                $subtype = $this->modx->getChunk('tpl.Document.DocTypeList',
                    array('types' => $subtype, 'id' => 'subtype', 'buttonid' => 'addDocSubForm'));
                $docId = $Doc->get('id');

                $response2 = $this->modx->runProcessor('getlist',
                    array('classKey' => $classKey, 'classKey_id' => $classKeyId, 'parent' => $docId),
                    array('processors_path' => $this->config['processorsPath']));
                $response2 = $this->modx->fromJSON($response2->response);

                if ($response2['total'] != 0) {
                    $subdocs = $this->modx->runSnippet('GetAllTypesDoc',
                        array('classKey' => $classKey, 'classKeyId' => $classKeyId, 'parent' => $docId));
                }

            } else {
                $newformdoc = 0;
            }
            return $this->success('', array(
                'result'    => $result,
                'filemax'   => $filemax,
                'pathfile'  => $pathfile,
                'thumbfile' => $thumbfile,
                'docfile'   => $docfile,
                'button'  => $newformdoc,
                'subtype' => $subtype,
                'doc_id'  => $docId,
                'subdocs' => $subdocs
            ));
        }


    }

    function removeDirectory($dir)
    {

        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {

                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }

        rmdir($dir);
    }

    public function DeleteDoc()
    {
        $data = $this->request;

        $properties = array('id' => $data['id']);
        $response = $this->modx->runProcessor('remove', $properties,
            array('processors_path' => $this->config['processorsPath']));
        if ($files = $this->modx->getCollection('DocumentFile', array('doc_id' => $data['id']))) {
            foreach ($files as $file) {
                $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->pathmed . $file->get('path_file');
                $this->removeDirectory($dir);

            }
        }
        if ($response->response['success']) {
            return $this->success($this->modx->lexicon('documents_remove'), '');
        } else {
            return $this->error($this->modx->lexicon('documents_err_remove'), '');
        }
    }


    public function DeleteDocFile()
    {
        $data = $this->request;
        if ($data['file']) {
            $Docfile = explode('/', $data['file']);
            $Docfile = array_pop($Docfile);
            if ($file = $this->modx->getObject('DocumentFile',
                array('doc_id' => $data['doc_id'], 'file_name' => $Docfile))
            ) {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $data['file'])) {

                    if (unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $data['file'])) {
                        if ($file->remove() != false) {
                            return $this->success($this->modx->lexicon('documents_file_remove'), '');
                        }
                    }
                }
            }
        }
        $img = explode('/', $data['img']);
        $dirname = dirname($data['img']);
        $img = array_pop($img);

        $imgthumb = 'thumb_' . $img;
        $imgthumb = $dirname . '/' . $imgthumb;
        if ($file = $this->modx->getObject('DocumentFile', array('doc_id' => $data['doc_id'], 'file_name' => $img))) {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $data['img']) and file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $imgthumb)) {
                if (unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $data['img']) and unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $imgthumb)) {
                    if ($file->remove() != false) {
                        return $this->success($this->modx->lexicon('documents_file_remove'), '');
                    }
                }
            }
        }
    }

}
