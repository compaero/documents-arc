<?php

class Document extends xPDOSimpleObject
{
    var $source;
    /* @var modMediaSource $mediaSource */
    public $mediaSource;

    public function save($cacheFlag = null)
    {

        $save = parent::save();
        $arrays = array();
        foreach ($this->_fieldMeta as $name => $field) {
            if (strtolower($field['phptype']) == 'json') {
                $tmp = $this->get($name);

                if (!empty($tmp) && is_array($tmp)) {
                    $arrays[$name] = $tmp;
                }

            }
        }

        $id = $this->get('id');
        $table = $this->xpdo->getTableName('DocumentField');
        $sql = 'DELETE FROM ' . $table . ' WHERE `doc_id` = ' . $id;
        $stmt = $this->xpdo->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        if (!empty($arrays)) {
            $values = array();

            foreach ($arrays as $tmp) {
                foreach ($tmp as $key => $value) {


                    $docfield = $this->xpdo->getObject('DocumentTypeField', array('field' => $key));
                    $docid = $docfield->get('id');
                    $values[] = '(' . $id . ',"' . $docid . '","' . $value . '")';

                }
            }

            if (!empty($values)) {
                $sql = 'INSERT INTO ' . $table . ' (`doc_id`,`field`,`value`) VALUES ' . implode(',', $values);
                $stmt = $this->xpdo->prepare($sql);
                $stmt->execute();
                $stmt->closeCursor();

            }
        }
        return $save;
    }


}