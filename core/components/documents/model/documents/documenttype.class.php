<?php

class DocumentType extends xPDOSimpleObject
{
    const maxAge = 'max_age';
    const MaxDocs = 'max_docs';
    const dateStart = 'date_issue';


    public function validate($Documents)

    {
        if ($Documents) {
            $validators = $this->get('validators');
            foreach ($validators as $k => $validate) {
                switch ($k) {
                    case DocumentType::maxAge:
                        if ($Documents->get('field')) {
                            foreach ($Documents->get('field') as $key => $field) {
                                if ($key == DocumentType::dateStart) {
                                    if (strtotime($field) + $validate < time() or strtotime($field) > time()) {
                                        return false;
                                    }
                                }

                            }
                        }

                        break;

                    case DocumentType::MaxDocs:
                        if ($docs = $this->xpdo->getCollection('Document',
                            array('type_id' => $Documents->get('type_id')))
                        ) {
                            if (count($docs) >= $validate) {
                                return false;
                            }
                        }


                        break;
                }

            }
        }


        return true;
    }
}