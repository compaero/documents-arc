<?php
if (!$Documents = $modx->getService('documents', 'Documents', $modx->getOption('documents_core_path', null, $modx->getOption('core_path') . 'components/documents/') . 'model/documents/', $scriptProperties)) {
    return 'Could not load Documents class!';
}


$classKey = $modx->getOption('classKey', $scriptProperties, '');
$classKeyId = $modx->getOption('classKeyId', $scriptProperties, '');
$Showparent = $modx->getOption('Showparent', $scriptProperties, 0);
$innerDocList = $modx->getOption('innerDocList', $scriptProperties, 'tpl.Document.innerDocList');
$DocList = $modx->getOption('DocList', $scriptProperties, 'tpl.Document.DocList');


$editedon = '';
$type = '';
if($Showparent){
    $Showparent = 1;
}
else{
    $Showparent = 0;
}

$response = $modx->runProcessor('getlist', array('classKey'=>$classKey,'classKey_id'=>$classKeyId,'showparent'=>$Showparent,'parent'=>$parent),
    array('processors_path' => MODX_CORE_PATH . 'components/documents/processors/'));
$results = json_decode($response->response, 1);
if ($results['total'] > 0) {
    foreach ($results['results'] as $result) {
        $createdon = date('d-m-Y H:i:s', $result['createdon']);
        if ($result['editedon'] != 0) {
            $editedon = date('d-m-Y H:i:s', $result['editedon']);
        }

        $typeid = $result['type_id'];

        if ($typeobj = $modx->getObject('DocumentType', array('id' => $typeid))) {
            $type = $typeobj->get('type');

        } else {
            $type = 'Неизвестный тип';
        }

        $id = $result['id'];

        $doclistrow .= $modx->getChunk($innerDocList, array(
            'type'     => $type,
            'id'       => $id,
            'date'     => $createdon,
            'user'     => $result['createdby'],
            'datedit'  => $editedon,
            'edituser' => $result['editedby']
        ));

    }

    $typeres = $modx->getChunk($DocList, array('row' => $doclistrow));
}

$Documents->loadJsCss();
return $typeres;