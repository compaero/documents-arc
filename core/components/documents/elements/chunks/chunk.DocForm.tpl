[[+types]]
<form id="docform" class="col-md-6" enctype="multipart/form-data">

    <div class="EditDocForm">

    </div>
    <span class="unlockDoc"><i class="fa fa-lock"></i></span>
    <input type="submit" id="submitDocForm" disabled class="btn green" value="Сохранить">
</form>
<div class="documentsFileUpload col-md-6">
    <div id="fotorama"

         data-click="false"
         class="fotorama"
         data-auto="false"  data-allowfullscreen="true">

    </div>
    <input type="file" filemax='' multiple id="DocumentsFileUpload">
    <div class="DocNoImageFile"></div>
</div>
<div class="SubDocuments col-md-12"></div>