<?php
$error_load_guests_service_msg = 'Could not load Documents service!';
if (!$Documents = $modx->getService('documents','documents',$modx->getOption('documents_core_path', null, $modx->getOption('core_path') . 'components/documents/') . 'model/documents/', $scriptProperties)) {
    $modx->log(xPDO::LOG_LEVEL_ERROR, $error_load_guests_service_msg);
    return;
}


switch ($modx->event->name) {
    case 'OnHandleRequest':
    case 'OnLoadWebDocument':


    $isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

    if (
        empty($_REQUEST['doc_action']) ||
        ($isAjax && $modx->event->name != 'OnHandleRequest') ||
        (!$isAjax && $modx->event->name != 'OnLoadWebDocument')
    ) {
        return;
    }

    $action = trim($_REQUEST['doc_action']);


            /** @var Documents $Documents */
            switch ($action) {
                case 'Doc/NewDoc':
                    $response = $Documents->NewDoc();
                    break;
                case 'Doc/Save':
                    $response = $Documents->SaveDoc();
                    break;
                case 'Doc/OpenDoc':
                    $response = $Documents->OpenDoc();
                    break;
                case 'Doc/DeleteDoc':
                    $response = $Documents->DeleteDoc();
                    break;
                case 'Doc/UploadFiles':
                    $response = $Documents->UploadFiles();
                    break;
                case 'Doc/DeleteDocFile':
                    $response = $Documents->DeleteDocFile();
                    break;




            }
    exit($response);


}