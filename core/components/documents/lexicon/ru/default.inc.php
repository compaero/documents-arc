<?php
include_once 'setting.inc.php';

$_lang['documents'] = 'Documents';
$_lang['documents_title'] = 'Documents';
$_lang['document_types'] = 'Типы документов';
$_lang['document_types_intro'] = 'На этой странице вы создаёте и редактируете типы документов';
$_lang['documents_documentsType_create'] = 'Cоздать тип документа';
$_lang['documents_documentsType_id'] = 'id';
$_lang['documents_documentsType_type'] = 'Тип';
$_lang['documents_documentsType_maxfiles'] = 'Максимальное кол-во файлов';
$_lang['documents_documentsType_parent'] = 'Родитель';
$_lang['documents_documentsType_validators'] = 'Валидаторы';
$_lang['documents_documentsType_name'] = 'Название типа';
$_lang['documents_documentsType_update'] = 'Изменить тип документа';
$_lang['documents_documentsType_remove'] = 'Удалить тип документа';
$_lang['documents_documentsType_remove_confirm'] = 'Тип документа будет удален';
$_lang['documents_documentsType_Field'] = 'Поля типа';

$_lang['document_type_fields'] = 'Поля типов документа';
$_lang['document_type_fields_intro'] = 'На этой странице вы создаёте и редактируете поля для типов документов';
$_lang['documents__type_field'] = 'Поле';
$_lang['documents__type_field_type'] = 'Тип поля';
$_lang['documents__type_field_create_link'] = 'Привязать поле';
$_lang['document_type_fields_Links'] = 'Доступные поля';

$_lang['documents__type_field_create'] = 'Cоздать поле';
$_lang['documents__type_field_window_create'] = 'Cоздать поле';
$_lang['documents__type_field_remove'] = 'Удалить поле';
$_lang['documents__type_field_remove_confirm'] = 'Поле будет удалено';
$_lang['documents__type_field_update'] = 'Изменить поле';

$_lang['documents__type_Links'] = 'Привязка типов к объектам';
$_lang['documents__type_Links_intro'] = 'На этой странице вы создаёте и редактируете связи типов с объектами';
$_lang['documents__type_Links_create'] = 'Привязать объект';
$_lang['documents_docLink_classKey'] = 'classKey';
$_lang['documents_docLink_id'] = 'Тип документа';
$_lang['documents_type_Links_remove'] = 'Удалить запись';
$_lang['documents_type_Links_remove_confirm'] = 'Запись будет удалена';








$_lang['documents_err_save_exist'] = 'Ошибка при сохранении Документа. Документ уже существует';
$_lang['documents_save'] = 'Документа сохранен';
$_lang['documents_cant_save'] = 'Не удалось сохранить документ';
$_lang['documents_files_save'] = 'Файлы загружены';
$_lang['documents_files_err_save'] = 'Не удалось загрузить Файлы';
$_lang['documents_remove'] = 'Документ удален';
$_lang['documents_err_remove'] = 'Не удалось удалить Документ';
$_lang['documents_file_remove'] = 'Файл удален';

$_lang['documents_field_description'] = 'Описание';
$_lang['documents_field_number'] = 'Номер';
$_lang['documents_field_series'] = 'Серия';
$_lang['documents_field_date_finish'] = 'Дата окончания';
$_lang['documents_field_date_issue'] = 'Дата выдачи';

$_lang['documents_empty_type'] = 'Не существующий тип';
