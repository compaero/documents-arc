<?php
include_once 'setting.inc.php';

$_lang['documents'] = 'Documents';
$_lang['documents_title'] = 'Documents';
$_lang['document_types'] = 'Documents types';
$_lang['document_types_intro'] = 'On this page, you create and edit document types';
$_lang['documents_documentsType_create'] = 'Create documents type';
$_lang['documents_documentsType_id'] = 'id';
$_lang['documents_documentsType_type'] = 'Type';
$_lang['documents_documentsType_maxfiles'] = 'max files';
$_lang['documents_documentsType_parent'] = 'parent';
$_lang['documents_documentsType_validators'] = 'validators';
$_lang['documents_documentsType_name'] = 'type name';
$_lang['documents_documentsType_update'] = 'update type';
$_lang['documents_documentsType_remove'] = 'remove type';
$_lang['documents_documentsType_remove_confirm'] = 'Type of document will be removed';
$_lang['documents_documentsType_Field'] = 'Fields type';

$_lang['document_type_fields'] = 'Fields type';
$_lang['document_type_fields_intro'] = '
On this page, you create and edit fields for the types of documents';
$_lang['documents__type_field'] = 'Field';
$_lang['documents__type_field_type'] = 'Field type';
$_lang['documents__type_field_create_link'] = 'add field';
$_lang['document_type_fields_Links'] = '
Available fields';

$_lang['documents__type_field_create'] = 'Create field';
$_lang['documents__type_field_window_create'] = 'Create field';
$_lang['documents__type_field_remove'] = 'Remove field';
$_lang['documents__type_field_remove_confirm'] = 'The field will be deleted';
$_lang['documents__type_field_update'] = 'Update field';

$_lang['documents__type_Links'] = '
Binding types of objects';
$_lang['documents__type_Links_intro'] = 'On this page, you create and edit an object type';
$_lang['documents__type_Links_create'] = 'Anchored objects';
$_lang['documents_docLink_classKey'] = 'classKey';
$_lang['documents_docLink_id'] = 'Type Documents';
$_lang['documents_type_Links_remove'] = 'remove entry';
$_lang['documents_type_Links_remove_confirm'] = 'The entry is removed';


$_lang['documents_err_save_exist'] = '
Failed to save the document . The document already exists';
$_lang['documents_save'] = 'Document saved';
$_lang['documents_cant_save'] = 'Failed to save the document';
$_lang['documents_files_save'] = 'Файлы загружены';
$_lang['documents_files_err_save'] = 'Files uploaded';
$_lang['documents_remove'] = 'The document is deleted';
$_lang['documents_err_remove'] = '
Failed to delete the document';
$_lang['documents_file_remove'] = 'File deleted';

$_lang['documents_field_description'] = 'Description';
$_lang['documents_field_number'] = 'number';
$_lang['documents_field_series'] = 'series';
$_lang['documents_field_date_finish'] = 'date finish';
$_lang['documents_field_date_issue'] = 'date issue';

$_lang['documents_empty_type'] = 'empty type';