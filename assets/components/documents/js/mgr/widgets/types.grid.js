Documents.grid.Type = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'documents-grid-documents';
	}
	Ext.applyIf(config, {
		url: Documents.config.connector_url,
		fields: this.getFields(config),
		columns: this.getColumns(config),
		tbar: this.getTopBar(config),
		sm: new Ext.grid.CheckboxSelectionModel(),
		baseParams: {
			action: 'mgr/documentstypes/getlist'
		},
		listeners: {
			//rowDblClick: function (grid, rowIndex, e) {
			//	var row = grid.store.getAt(rowIndex);
			//	this.updateItem(grid, e, row);
			//}

		},
		viewConfig: {
			forceFit: true,
			enableRowBody: true,
			autoFill: true,
			showPreview: true,
			scrollOffset: 0,
			//getRowClass: function (rec, ri, p) {
			//	return !rec.data.active
			//		? 'documents-grid-row-disabled'
			//		: '';
			//}
		},
		paging: true,
		remoteSort: true,
		autoHeight: true,
		save_action: 'mgr/documentstypes/update',
		autosave: true,
		plugins: this.exp

	});
	Documents.grid.Type.superclass.constructor.call(this, config);

	// Clear selection on grid refresh
	this.store.on('load', function () {
		if (this._getSelectedIds().length) {
			this.getSelectionModel().clearSelections();
		}
	}, this);
};
Ext.extend(Documents.grid.Type, MODx.grid.Grid, {
	windows: {},

	getMenu: function (grid, rowIndex) {
		var m = [];
		m.push({
			text: _('documents_documentsType_update')
			, handler: this.updateItem
		});
		m.push('-');
		m.push({
			text: _('documents_documentsType_remove')
			, handler: this.removeItem
		});
		m.push('-');
		m.push({
			text: _('documents_documentsType_Field')
			, handler: this.TypeFields
		});
		this.addContextMenuItem(m);
	},
	TypeFields: function (btn, e, row) {
		if (typeof(row) != 'undefined') {
			this.menu.record = row.data;
		}
		else if (!this.menu.record) {
			return false;
		}
		var id = this.menu.record.id;

		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/documentstypesfields/getlist',
				id: id
			},
			listeners: {
				success: {
					fn: function (r) {
						var w = MODx.load({
							xtype: 'documents-documents-Type-Field-window-grid',
							id: Ext.id(),
							record: id,
							listeners: {
								success: {
									fn: function () {
										this.refresh();
									}, scope: this
								}
							}
						});
						w.reset();
						w.setValues(r.object);
						w.show(e.target);
					}, scope: this
				}
			}
		});
	},
	createItem: function (btn, e) {
		var w = MODx.load({
			xtype: 'documents-documentsType-window-create',
			id: Ext.id(),
			listeners: {
				success: {
					fn: function () {
						console.log(this);
						this.refresh();
					}, scope: this
				}
			}
		});
		w.reset();
		w.setValues({active: true});
		w.show(e.target);
	},

	updateItem: function (btn, e, row) {

		if (typeof(row) != 'undefined') {
			this.menu.record = row.data;
		}
		else if (!this.menu.record) {
			return false;
		}
		var id = this.menu.record.id;

		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/documentstypes/get',
				id: id
			},
			listeners: {
				success: {
					fn: function (r) {
						var w = MODx.load({
							xtype: 'documents-documentsType-window-update',
							id: Ext.id(),
							record: r,
							listeners: {
								success: {
									fn: function () {
										this.refresh();
									}, scope: this
								}
							}
						});
						w.reset();
						w.setValues(r.object);
						w.show(e.target);
					}, scope: this
				}
			}
		});
	},

	removeItem: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.msg.confirm({
			title: ids.length > 1
				? _('documents_documentsType_remove')
				: _('documents_documentsType_remove'),
			text: ids.length > 1
				? _('documents_documentsType_remove_confirm')
				: _('documents_documentsType_remove_confirm'),
			url: this.config.url,
			params: {
				action: 'mgr/documentstypes/remove',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function (r) {
						this.refresh();
					}, scope: this
				}
			}
		});
		return true;
	},

	disableItem: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/documentstypes/disable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	enableItem: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/documentstypes/enable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	getFields: function (config) {
		return ['id', 'type', 'max_files', 'parent', 'validators'];
	},

	getColumns: function (config) {
		return [{
			header: _('documents_documentsType_id'),
			dataIndex: 'id',
			sortable: true,
			width: 70
		}, {
			header: _('documents_documentsType_type'),
			dataIndex: 'type',
			sortable: true,
			width: 200,
			editor: {xtype: 'textfield'}
		}, {
			header: _('documents_documentsType_maxfiles'),
			dataIndex: 'max_files',
			sortable: false,
			width: 70,
			editor: {xtype: 'textfield'}
		}, {
			header: _('documents_documentsType_parent'),
			dataIndex: 'parent',

			sortable: true,
			width: 70,
			typeAhead: true,
			editor: {xtype: 'documents-combo-types'},
			//renderer: this.getparent


		}, {
			header: _('documents_documentsType_validators'),
			dataIndex: 'validators',
			sortable: false,
			width: 100,



			//editor: {xtype: 'documents-combo-json', renderer: this.renderJson},

			renderer: this.renderJson
		}];
	},
	getparent: function (value,row,props,store) {

var url =Documents.config.connector_url;
		console.log(store);
		//console.log(props.data.parent);
		var id = value;
		var type ='';

if(id !=0){
	MODx.Ajax.request({
		url: url,
		params: {
			action: 'mgr/documentstypes/get',
			id: id
		},
		listeners: {
			success: {fn: function(response) {


				//var el = Ext.getCmp(store);
				this.setValue(response.object.type);
				console.log(response.object.type);
				console.log(this);

			}, scope: this}
		}
	});

}





	},
	renderJson: function (value, props, row) {
		if (value != '') {
			return JSON.stringify(value);
		}
		else {
			return '';
		}


	},
	getTopBar: function (config) {
		return [{
			text: '<i class="icon icon-plus"></i>&nbsp;' + _('documents_documentsType_create'),
			handler: this.createItem,
			scope: this
		}, '->', {
			xtype: 'textfield',
			name: 'query',
			width: 200,
			id: config.id + '-search-type',
			emptyText: _('documents_grid_search'),
			listeners: {
				render: {
					fn: function (tf) {
						tf.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
							this._doSearch(tf);
						}, this);
					}, scope: this
				}
			}
		}, {
			xtype: 'button',
			id: config.id + '-search-clear',
			text: '<i class="icon icon-times"></i>',
			listeners: {
				click: {fn: this._clearSearch, scope: this}
			}
		}];
	},

	onClick: function (e) {

		var elem = e.getTarget();
		if (elem.nodeName == 'BUTTON') {

			var row = this.getSelectionModel().getSelected();
			if (typeof(row) != 'undefined') {
				var action = elem.getAttribute('action');
				if (action == 'showMenu') {
					var ri = this.getStore().find('id', row.id);
					return this._showMenu(this, ri, e);
				}
				else if (typeof this[action] === 'function') {
					this.menu.record = row.data;
					return this[action](this, e);
				}
			}
		}
		return this.processEvent('click', e);
	},

	_getSelectedIds: function () {
		var ids = [];
		var selected = this.getSelectionModel().getSelections();

		for (var i in selected) {
			if (!selected.hasOwnProperty(i)) {
				continue;
			}
			ids.push(selected[i]['id']);
		}

		return ids;
	},

	_doSearch: function (tf, nv, ov) {
		this.getStore().baseParams.query = tf.getValue();
		this.getBottomToolbar().changePage(1);
		this.refresh();
	},

	_clearSearch: function (btn, e) {
		this.getStore().baseParams.query = '';
		Ext.getCmp(this.config.id + '-search-field').setValue('');
		this.getBottomToolbar().changePage(1);
		this.refresh();
	}
});
Ext.reg('document-grid-types', Documents.grid.Type);


