/**
 * Created by keselek on 24.03.2016.
 */
Documents.window.CreateField = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'documents__type_field_window_create';
    }
    Ext.applyIf(config, {
        title: _('documents__type_field_window_create'),
        width: 550,
        autoHeight: true,
        url: Documents.config.connector_url,
        action: 'mgr/documentsfields/create',
        fields: this.getFields(config),
        keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    Documents.window.CreateField.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.CreateField, MODx.Window, {

    getFields: function (config) {
        return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
            {
                layout: 'column'
                , border: false
                , anchor: '100%'
                , items: [{
                columnWidth: .5
                , layout: 'form'
                , defaults: {msgTarget: 'under'}
                , border: false
                , items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: _('documents__type_field'),
                        name: 'field',
                        id: config.id + '-field',
                        anchor: '99%'
                    }
                ]
            }, {
                columnWidth: .5
                , layout: 'form'
                , defaults: {msgTarget: 'under'}
                , border: false
                , items: [
                    {
                        xtype: 'documents-combo-types-field',
                        fieldLabel: _('documents__type_field_type'),
                        name: 'type',
                        id: config.id + '-type',
                        anchor: '99%',
                    }]
            }]
            },
        ];
    },

    loadDropZones: function () {
    }

});
Ext.reg('documents-documentsTypeField-window-create', Documents.window.CreateField);


Documents.window.UpdateField = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'documents-documentsTypeField-window-update';
    }
    Ext.applyIf(config, {
        title: _('documents__type_field_update'),
        width: 550,
        autoHeight: true,
        url: Documents.config.connector_url,
        action: 'mgr/documentsfields/update',
        fields: this.getFields(config),
        keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    Documents.window.UpdateField.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.UpdateField, MODx.Window, {

    getFields: function (config) {
        return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
            {
                layout: 'column'
                , border: false
                , anchor: '100%'
                , items: [{
                columnWidth: .5
                , layout: 'form'
                , defaults: {msgTarget: 'under'}
                , border: false
                , items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: _('documents__type_field'),
                        name: 'field',
                        id: config.id + '-field',
                        anchor: '99%'
                    }
                ]
            }, {
                columnWidth: .5
                , layout: 'form'
                , defaults: {msgTarget: 'under'}
                , border: false
                , items: [
                    {
                        xtype: 'documents-combo-types-field',
                        fieldLabel: _('documents__type_field_type'),
                        name: 'type',
                        id: config.id + '-type',
                        anchor: '99%',
                    }]
            }]
            }
        ];
    },

    loadDropZones: function () {
    },


});
Ext.reg('documents-documentsTypeField-window-update', Documents.window.UpdateField);
